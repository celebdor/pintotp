// Copyright © 2018 Antoni Segura Puimedon <celebdor@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License. 
package cmd

import (
	"fmt"
	"os"
	"path"
	"strings"
	"time"

	"github.com/mitchellh/go-homedir"
        "github.com/pquerna/otp"
        "github.com/pquerna/otp/totp"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/celebdor/pintotp/pkg/pass"
	"golang.org/x/crypto/ssh/terminal"
	"github.com/atotto/clipboard"
)

var (
	cfgFile string
)

var showCmd = &cobra.Command{
	Use: "pintotp",
	Short: "Generates a new PIN + TOTP combination",
	Run: func(cmd *cobra.Command, args []string) {
		defaultToken := viper.Get("DefaultToken")
		fmt.Fprintln(os.Stderr, "Generating pin + otp for:", defaultToken)
		tokenPinKey := fmt.Sprintf("tokens.%s.pinPassPath", defaultToken)
		tokenUrlKey := fmt.Sprintf("tokens.%s.urlPassPath", defaultToken)

		fmt.Fprintln(os.Stderr, "Enter gpg passphrase for key", defaultToken)
		bytepass, err := terminal.ReadPassword(0)
		gpgPassphrase := string(bytepass)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		tokenPin, err := pass.GetPass(viper.GetString(tokenPinKey), gpgPassphrase)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		tokenURL, err := pass.GetPass(viper.GetString(tokenUrlKey), gpgPassphrase)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		key, err := otp.NewKeyFromURL(tokenURL)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		currentOTP, err := totp.GenerateCode(key.Secret(), time.Now())
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		pinAndOTP := fmt.Sprintf("%s%s", strings.TrimRight(tokenPin, "\n"), currentOTP)

		if viper.GetBool("useClipboard") {
			clipboard.WriteAll(pinAndOTP)
		} else {
			fmt.Println(pinAndOTP)
		}
	},
}

func init() {
	cobra.OnInitialize(initConfig)
	showCmd.PersistentFlags().BoolP("clipboard", "c", false, "Copy to clipboard")
	viper.BindPFlag("useClipboard", showCmd.PersistentFlags().Lookup("clipboard"))
	showCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.config/pintotp/config.yaml)")
}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		defaultCfgHome := path.Join(home, ".config", "pintotp")
		viper.SetDefault("XDG_CONFIG_HOME", defaultCfgHome)
		viper.AddConfigPath(viper.GetString("XDG_CONFIG_HOME"))
		viper.SetConfigName("config")
	}

	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		fmt.Println("Can't read config:", err)
		os.Exit(1)
	}
}

func Execute() {
	if err := showCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}
