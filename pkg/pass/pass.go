// Copyright © 2018 Antoni Segura Puimedon <celebdor@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License. 
package pass

import (
	"bytes"
	"fmt"
	"os/exec"
	"path"
	"strings"

	"github.com/mitchellh/go-homedir"
)

func getPassStorePath() (string, error) {
	home, err := homedir.Dir()
	if err != nil {
		return "", err
	}
	return path.Join(home, ".password-store"), nil
}

func GetPass(entry string, passphrase string) (string, error) {
	passStorePath, err := getPassStorePath()
	if err != nil {
		return "", err
	}

	passPath := passStorePath + "/" + entry + ".gpg"
	cmd := exec.Command("gpg", "--batch", "--passphrase-fd", "0", "--decrypt", passPath)
	var gpgOut bytes.Buffer
	cmd.Stdout = &gpgOut

	cmd.Stdin = strings.NewReader(passphrase)

	err = cmd.Run()
	if err != nil {
		fmt.Println(cmd.Path, cmd.Args)
		return "", err
	}

        return gpgOut.String(), nil
}
